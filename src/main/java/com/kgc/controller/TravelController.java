package com.kgc.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.TravelDetails;
import com.kgc.service.TravelService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
@ResponseBody
@RequestMapping("travel")
@Controller
public class TravelController {
    @Autowired
    private TravelService travelService;



    @RequestMapping("list")
    public PageInfo getTravelList(@RequestParam(value = "pn",defaultValue = "1") Integer pn, @RequestParam(value = "ps",defaultValue = "5") Integer ps){
        PageHelper.startPage(pn,ps);
        List<TravelDetails> details = travelService.getTravelList();

        PageInfo info = new PageInfo(details,5);
        return info;
    }

    @RequestMapping("list1")
    public PageInfo getTravelList1(@RequestParam(value = "pn",defaultValue = "1") Integer pn, @RequestParam(value = "ps",defaultValue = "5") Integer ps){
        PageHelper.startPage(pn,ps);
        List<TravelDetails> details = travelService.getTravelList1();

        PageInfo info = new PageInfo(details,5);
        return info;
    }
    @RequestMapping("list2")
    public PageInfo getTravelList2(@RequestParam(value = "pn",defaultValue = "1") Integer pn, @RequestParam(value = "ps",defaultValue = "5") Integer ps){
        PageHelper.startPage(pn,ps);
        List<TravelDetails> details = travelService.getTravelList2();

        PageInfo info = new PageInfo(details,5);
        return info;
    }



    @RequestMapping("total")
    public  int testGetTotal(){
        return travelService.getTotal();
    }




    @RequestMapping("details")
    public  TravelDetails testDetailsById(int id){
        return travelService.getDetailsById(id);

    }



    @RequestMapping("search")
    public PageInfo testDetailsByDetails(@RequestParam(value = "pn",defaultValue = "1") Integer pn, @RequestParam(value = "ps",defaultValue = "5")Integer ps,String details){
        System.out.println(travelService.getDetailsBySearch(details));
        PageHelper.startPage(pn,ps);
        List<TravelDetails> search = travelService.getDetailsBySearch(details);
        PageInfo info = new PageInfo(search,5);
        return info;
    }




    @RequestMapping("favorite")
    public int testFavorite(TravelDetails travelDetails){
        return travelService.addFavorite(travelDetails);
    }

    @RequestMapping("favoritelist")
    public PageInfo testFavoriteList(@RequestParam(value = "pn", defaultValue = "1") Integer pn, @RequestParam(value = "ps",defaultValue = "10" ) Integer ps, int uid){
        PageHelper.startPage(pn,ps);
        List<TravelDetails> favoriteList = travelService.getFavoriteListById(uid);
        PageInfo info = new PageInfo(favoriteList,5);
        return info;
    }

    @RequestMapping("delete")
    public boolean testDeleteFavorite(int id,int uid){
        if (travelService.deleteFavorite(id,uid)>0){
            return true;
        }else {
            return false;
        }

    }


}
