package com.kgc.service.impl;

import com.kgc.entity.User;
import com.kgc.mapper.UserMapper;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;


    @Override
    public int registerUser(User user) {
        return userMapper.registerUser(user);
    }

    @Override
    public User userLogin(String username, String password) {
        return userMapper.userLogin(username,password);
    }

    @Override
    public List<User> getUserList(int id) {
        return userMapper.getUserList(id);
    }

    @Override
    public boolean update(User user) {
        return userMapper.update(user)==1 ? true : false;
    }
}
