package com.kgc.service.impl;

import com.kgc.entity.TravelDetails;
import com.kgc.mapper.TravelMapper;
import com.kgc.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TravelServiceImpl implements TravelService {
  @Autowired
  private TravelMapper travelMapper;

    @Override
    public List<TravelDetails> getTravelList() {
        return travelMapper.getTravelList();
    }

    @Override
    public List<TravelDetails> getTravelList1() {
        return travelMapper.getTravelList1();
    }

    @Override
    public List<TravelDetails> getTravelList2() {
        return travelMapper.getTravelList2();
    }






    @Override
    public int getTotal() {
        return travelMapper.getTotal();
    }

  @Override
  public TravelDetails getDetailsById(int id) {
    return travelMapper.getDetailsById(id);
  }

  @Override
  public List<TravelDetails> getDetailsBySearch(String details) {
    return travelMapper.getDetailsBySearch(details);
  }

  @Override
  public int addFavorite(TravelDetails travelDetails) {
    return travelMapper.addFavorite(travelDetails);
  }

  @Override
  public List<TravelDetails> getFavoriteListById(int uid) {
    return travelMapper.getFavoriteListById(uid);
  }

  @Override
  public int deleteFavorite(int id, int uid) {
    return travelMapper.deleteFavorite(id,uid);
  }
}
