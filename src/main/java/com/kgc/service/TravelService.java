package com.kgc.service;

import com.kgc.entity.TravelDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TravelService {
    List<TravelDetails> getTravelList();

    List<TravelDetails> getTravelList1();

    List<TravelDetails> getTravelList2();


    int getTotal();

    TravelDetails getDetailsById(int id);

    List<TravelDetails> getDetailsBySearch(String details);

    int addFavorite(TravelDetails travelDetails);

    List<TravelDetails> getFavoriteListById( int uid);


    int deleteFavorite(int id ,int uid);
}
