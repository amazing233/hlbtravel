package com.kgc.mapper;

import com.kgc.entity.Orders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrdersMapper {
    //增加订单
    int addOrders(@Param("orders") Orders orders);

    //根据用户查询订单信息
    List<Orders> getOrdersList(@Param("uid") int uid);

    //删除订单
    int deleteOrders(@Param("id")String id);



}
