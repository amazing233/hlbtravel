package com.kgc.mapper;

import com.kgc.entity.TravelDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TravelMapper {
    List<TravelDetails> getTravelList();

    List<TravelDetails> getTravelList1();

    List<TravelDetails> getTravelList2();

    //查询数据总条数
    int getTotal();

    //根据id查询旅游的详情信息
    TravelDetails getDetailsById(@Param("id") int id);

    //根据输入内容查询旅游详情
    List<TravelDetails> getDetailsBySearch(@Param("details") String details);

    //插入收藏信息
    int addFavorite(@Param("travelDetails") TravelDetails travelDetails);

    //根据用户查询用户收藏
    List<TravelDetails> getFavoriteListById(@Param("uid") int uid);

    //删除收藏信息（根据用户id）
    int deleteFavorite(@Param("id") int id ,@Param("uid") int uid);




}
