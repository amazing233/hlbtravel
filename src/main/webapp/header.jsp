<%@ taglib prefix="v-on" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="js/jquery-3.3.1.js" type="text/javascript"></script>
<script src="js/vue.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/common.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="js/include.js">
<link rel="stylesheet" href="src/jquery.typeahead.css">
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script src="src/jquery.typeahead.js"></script>

<script type="text/javascript">
    $(function () {
        $("a[class=search-button]").click(function () {
            var details = $("input[class=search_input]").val();
            window.location.href = "search_list.jsp?details=" + details;
        })
    });

    function out() {

            var result = confirm("确定要退出吗？");
             if (result) {
            window.location.href = "user/tologout";
             }	

    }
    function changeA(){

document.getElementById("menuA").style.background='orange';

document.getElementById("menuB").style.background='#FFC900';

}

function changeB(){

document.getElementById("menuB").style.background='orange';

document.getElementById("menuA").style.background='#FFC900';

}
</script>
<%--<style>
    .btn {
        color: blue;
        background-color: #FFFFFF;
        border: 0px none;
        font-family: "宋体";
        font-size: 15px;
        text-decoration: underline;
    }

    .btn:hover {
        color: red;
        border: none;
        cursor: hand;
        cursor: pointer;
        text-decoration: underline;
    }

    .btn:focus {
        outline: none;
    }
</style>--%>
<!-- 头部 start -->
<header id="header">
    <div class="top_banner">
        <span class="login_left">去见证更美的世界</span>
        <div class="shortcut">
            <!-- 未登录状态  -->

            <div class="login_out">
                <a href="login.jsp">登录</a>
                <a href="register.jsp">注册</a>
            </div>
            <!-- 登录状态  -->
            <div class="login">
                <span>欢迎 : ${sessionScope.user.username}</span>
               
                <a href="personal.jsp" class="collection">个人中心</a>
                <a href="orders.jsp" style="color: black;" >我的订单</a>
                <a href="" onclick="out()">退出</a>
            </div>
        </div>
    </div>
    <div class="header_wrap">
        <div class="topbar">
            <div class="logo">
                <a href="index.jsp"><img src="images/logo1.jpg" alt=""></a>
            </div>

            <div class="sousuo">
                <form style="position:absolute;z-index:100">
                    <div class="typeahead__container">
                        <div class="typeahead__field">
							<span class="typeahead__query">
								<input placeholder="请输入路线名称" class="search_input" name="q" type="search" autofocus
                                       autocomplete="off">
							</span>
                            <span class="typeahead__button">
								<button type="submit">
									<span class="typeahead__search-icon"></span>
								</button>
							</span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="hottel">
                <div class="hot_tel">
                    <p class="hot_time">客服热线(9:00-6:00)</p>
                    <p class="hot_num">400-618-9090</p>
                </div>
                <div class="hot_pic">
                    <img src="images/hot_tel.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</header>
<!-- 头部 end -->
<!-- 首页导航 -->
<div class="navitem">
    <ul class="nav" >
        <li class="nav-active" id="menuA"><a href="#" onclick="changeA()">首页</a></li>
        <li class="nav-active" id="menuB"><a href="#" onclick="changeB()">出境游</a></li>
        <li class="nav-active"><a href="route_list1.jsp">国内游</a></li>
        <li class="nav-active"><a href="route_list.jsp">全球自由行</a></li>
    </ul>
</div>

<script>
    var data = {
        Chinesewine: ['苏州+乌镇+杭州3日2晚跟团游',
            '上海+乌镇+杭州3日2晚跟团游',
            '黄山风景区3日2晚跟团游',
            '江西婺源+三清山3日2晚跟团游',
            '普陀山+宁波3日2晚跟团游',
            '宏村+西递2日1晚跟团游',
            '横店影视城2日跟团游',
            '山东曲阜+泰山+济南3日2晚跟团游',
            '泰国曼谷+芭堤雅6日5晚跟团游',
            '印度尼西亚巴厘岛7日5晚私家团',
            '新加坡6日5晚跟团游',
            '泰国普吉岛6日5晚跟团游',
            '越南芽庄6日5晚半自助游',
            '日本北海道6日5晚半自助游',
            '美国东西海岸+夏威夷+尼亚加拉瀑布15日跟团游',
            '埃及阿斯旺+卢克索+红海Red Sea+开罗+亚历山大12日跟团游',
            '泰国+新加坡+马来西亚10日9晚跟团游',
            '日本青森县+轻井泽町5日自由行',
            '安吉涵田度假村+含半山竹韵温泉票2张+2018年新开业,1-3晚任选',
            '浙江丽水3日2晚跟团游',
            '西塘2日1晚跟团游',
            '昆明+大理+丽江+玉龙雪山6日5晚跟团游',
            '西安+秦始皇兵马俑博物馆+华清池+华山+大明宫+大雁塔5日跟团游',
            '苏州+周庄+同里2日1晚跟团游',
            '日本大阪+京都+箱根+东京6日跟团游',
            '澳大利亚-凯恩斯-墨尔本10日游',
            '新西兰-南北岛冰川14日游',
            '迪拜+阿布扎比+沙迦4晚6日游',
            '埃及全景10日游',
            '以色列约旦10日7晚游',
            '塞班岛肯辛顿酒店5晚7日自由行',
            '毛里求斯Le Meridien艾美度假村5晚8日自由行',
            '法国意大利瑞士12日游',
            '西班牙+葡萄牙11-12日游',
            '奥地利+捷克+匈牙利11日游',
            '美国奥兰多一地7-9日自由行',
            '美国西海岸9-10日游',
            '美国西海岸-旧金山-拉斯-洛杉矶机票+当地13日半自助游',
            '斯里兰卡机票+当地5晚7日游',
            '柬埔寨-吴哥4晚6日游',
            '新加坡花园城市滨海湾 5日经典游',
            '摩洛哥10日游',
            '马尔代夫鲁滨逊岛Robinson5晚7日自由行',
            '拉萨-林芝-大峡谷-巴松措-布达拉宫-大昭寺双飞7日游',
            '呼伦贝尔-冷极村-驯鹿部落-根河湿地-满洲里双飞双卧5日游',
            '桂林+龙脊梯田+大漓江+阳朔双飞5日',
            '厦门-鼓浪屿-云水谣土楼双飞5日游',
            '北京双高5日游',
            '新疆+胡杨林+库车+库尔勒3飞8日游',
            '兰州+张掖丹霞+敦煌莫高窟+吐鲁番+新疆沙漠双飞7日游',
            '西宁-青海湖-茶卡盐湖-德令哈-敦煌-嘉峪关-张掖双飞6日游',
            '海南三亚双飞5日游',
            '乌镇-西塘2日游']
    };
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".search_input",
        minLength: 1,
        order: "asc",
        group: true,
        maxItemPerGroup: 3,
        groupOrder: function () {

            var scope = this,
                sortGroup = [];

            for (var i in this.result) {
                sortGroup.push({
                    group: i,
                    length: this.result[i].length
                });
            }

            sortGroup.sort(
                scope.helper.sort(
                    ["length"],
                    false, // false = desc, the most results on top
                    function (a) {
                        return a.toString().toUpperCase()
                    }
                )
            );

            return $.map(sortGroup, function (val, i) {
                return val.group
            });
        },
        hint: true,
        dropdownFilter: "All",
        href: "https://en.wikipedia.org/?title={{display}}",
        template: "{{display}}, <small><em>{{group}}</em></small>",
        emptyTemplate: "no result for {{query}}",
        source: {
            Chinesewine: {
                data: data.Chinesewine
            },
        },
        callback: {
            onClickAfter: function (node, a, item, event) {
                event.preventDefault();

                var r = confirm("You will be redirected to:\n" + item.href + "\n\nContinue?");
                if (r == true) {
                    window.location.href='route_detail.jsp?id=43';
                }

                $('.js-result-container').text('');

            },
            onResult: function (node, query, obj, objCount) {

                console.log(objCount)

                var text = "";
                if (query !== "") {
                    text = objCount + ' elements matching "' + query + '"';
                }
                $('.js-result-container').text(text);

            }
        },
        debug: true
    });
</script>
