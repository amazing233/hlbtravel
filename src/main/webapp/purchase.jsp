<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>旅游网</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<style>
    .message {
        border-collapse:collapse;
        width: 700px;
    }
    .message td{
        line-height: 60px;
    }


    #size{
        width: 1270px;
    }
    #neirong{
        height: 800px;
    }
    .biaoti{
        font-size:24px;

    }
    table{
        margin: 10px;
        line-height: 50px;
    }
    .depart_detail{
        font-size: 12px;
    }
    .input_m{
        border: black 0.0625rem solid;
        width: 160px;
        height: 40px;
    }
    button {
        background-color: orange; /* Green */
        border: none;
        color: white;
        padding: 3px 27px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 15px;
    }
    .kfc{
        border:1px solid #ccc;
    }

</style>


<body>
<%
         int price = Integer.parseInt(request.getParameter("price"));
         String person = request.getParameter("person");
         String time =request.getParameter("time");
         String details = request.getParameter("detail");
         session.setAttribute("price",price);
         session.setAttribute("person",person);
         session.setAttribute("time",time);
         session.setAttribute("details",details);




%>



<div id="header"></div>
<div id="neirong">
    <from>
        <table>
            <tr>
                <td>
                    <img src="images/1572939112.png" id="size">
                </td>
            </tr>
            <tr>
                <td>
                    <p class="biaoti"><%=details%></p>
                    <strong><p class="depart_detail"><span>出发日期：<%=time%></span>&nbsp;<span><%=person%>人</span></p></strong>
                </td>
            </tr>
            <tr>
                <td>
                    <h1 style="color: red;">总金额:￥<span id="price"><%=price%></span></h1>

                </td>


            </tr>




            <tr>
                <td>
                    <h3>联系人信息<span class="linkman_tips">为方便携程及时与您联系，请准确填写联系人信息（手机号码 身份证号码）</span></h3>
                </td>
            </tr>
        </table>
        <table class="message">
            <tr>
                <td class="kfc">
                    <label class="product_label">姓名:</label><span class="frm_required">*</span>
                </td>
                <td class="kfc">
                    <input name="ContactName" class="input_m" type="text" placeholder="请输入联系人姓名" value="" data-valid="false"
                           data-message="请填写姓名" data-container="uid-0">
                </td>
                <td>
                    //提示框
                </td>
            </tr>
            <tr>
                <td class="kfc">
                    <label class="product_label">手机号码:</label>
                    <span class="frm_required">*</span>
                    <select>
                        <option style="display: none;" value="86" selected="">中国86</option>
                    </select>
                </td>
                <td class="kfc">
                    <input name="Mobile" class="input_m" type="text" placeholder="必填，用于接收短信" value="" data-valid="false"
                           data-message="请填写手机号码" data-container="uid-1">
                </td>
                <td>
                    //提示框
                </td>
            </tr>
            <tr>
                <td class="kfc">
                    <label class="product_label">身份证号:</label>
                    <span class="frm_required">*</span>
                </td>
                <td class="kfc">
                    <input name="sfz" class="input_m" type="text" placeholder="必填，用于查询您的用户信息!" value="" autocomplete="off"
                           data-valid="true" data-message="请填写身份证号" data-container="uid-2"><span class="hrs"></span>
                </td>
                <td>
                    //提示框
                </td>
            </tr>
            <tr>
                <td class="kfc">
                    <button value="确认购买" class="shop">确认购买</button>
                </td>
                <td class="kfc">
                    <button value="取消购买">取消购买</button>
                </td>
                <td>

                </td>
            </tr>
        </table>
    </from>
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    //获取当前价格的方法
    $("button[class=shop]").click(function() {


        var in_name = $("input[name=ContactName]").val();
        var in_money = $("span[id=price]").html();
        if (confirm("确认支付订单吗？")) {
            window.location.href = "to_alipay.do?in_name=" + in_name + "&in_phone=18852734236"
                + "&in_money=" + in_money+"&details=${sessionScope.details}&time=${sessionScope.time}&person=${sessionScope.person}";
        }
    });



</script>




</body>
</html>
